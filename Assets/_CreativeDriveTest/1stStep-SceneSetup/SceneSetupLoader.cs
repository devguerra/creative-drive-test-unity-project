﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class SceneSetupLoader : MonoBehaviour
{
    private const string CONFIG_FILE_URI = @"https://s3-sa-east-1.amazonaws.com/static-files-prod/unity3d/models.json";
    private const string DEBUG_HEADER = "<b>LoadConfigurationFile</b>";
    private const string JSON_MODELS_FIELD = "models";
    private const string WAIT_MESSAGE = "Loading Scene Configuration";

    public delegate void SceneSetupLoaderDelegate(SetupData setupData);

    public event SceneSetupLoaderDelegate OnSetupLoaded;

    private IEnumerator Start()
    {
        yield return LoadConfigurationFile();
    }

    private IEnumerator LoadConfigurationFile()
    {
        WaitScreen.Instance.Show(WAIT_MESSAGE);

        string localFilePath = SceneSerializer.GetSceneConfigurationFilePath();

        if(File.Exists(localFilePath))
        {
            LoadConfigurationFileFromFileSystem();
        }
        else
        {
            yield return LoadConfigurationFileFromWeb();
        }

        WaitScreen.Instance.Hide();
    }

    private IEnumerator LoadConfigurationFileFromWeb()
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(CONFIG_FILE_URI))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError)
            {
                Debug.LogError($"{DEBUG_HEADER} Data couldn't be loaded, error: {webRequest.error}");
            }
            else
            {
                string json = webRequest.downloadHandler.text;

                // necessary to remove a byte marker that invalidates the JSON when parsing
                json = SanitizeJson(json);

                Debug.Log($"{DEBUG_HEADER} Data successfully loaded from {CONFIG_FILE_URI}:\n{json}");

                SetupData setupData = BuildSetupFile(json);

                OnSetupLoaded?.Invoke(setupData);
            }
        }
    }

    private void LoadConfigurationFileFromFileSystem()
    {
        string path = SceneSerializer.GetSceneConfigurationFilePath();

        string json = File.ReadAllText(path);

        Debug.Log($"{DEBUG_HEADER} Data successfully loaded from {path}:\n{json}");

        SetupData setupData = BuildSetupFile(json);

        OnSetupLoaded?.Invoke(setupData);
    }

    private SetupData BuildSetupFile(string json)
    {
        var jsonObject = JObject.Parse(json);

        var modelArray = jsonObject.SelectToken(JSON_MODELS_FIELD)
            .Select(jt => jt.ToObject<ModelSetup>())
            .ToList();

        SetupData setupData = new SetupData(modelArray);

        return setupData;
    }

    private string SanitizeJson(string json)
    {
        return json.Trim(new char[] { '\uFEFF' });
    }
}
