﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
[JsonObject(MemberSerialization = MemberSerialization.OptIn)]
public class SetupData
{
    [JsonProperty("models")]
    private readonly List<ModelSetup> models;
    public ModelSetup[] Models
    {
        get => models.ToArray();
    }

    public SetupData(List<ModelSetup> models)
    {
        this.models = models;
    }

    public void UpdateData(ModelSetup[] models)
    {
        this.models.Clear();

        this.models.AddRange(models);
    }
}
