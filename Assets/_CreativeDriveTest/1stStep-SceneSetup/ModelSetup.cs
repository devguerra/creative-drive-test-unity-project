﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;

[Serializable]
[JsonObject(MemberSerialization = MemberSerialization.OptIn)]
public class ModelSetup
{
    #region Raw JSON Fields
    [JsonProperty("position", Order = 1)]
    [SerializeField]
    private float[] positionRaw;
    [JsonProperty("rotation", Order = 2)]
    [SerializeField]
    private float[] rotationRaw;
    [JsonProperty("scale", Order = 3)]
    [SerializeField]
    private float[] scaleRaw;
    #endregion

    #region Properties
    [JsonProperty("materialData", Order = 4)]
    [SerializeField]
    private MaterialData materialData;
    public MaterialData MaterialData
    {
        get => materialData;
    }

    [JsonProperty("name", Order = 0)]
    [SerializeField]
    private string name;
    public string Name
    {
        get => name;
    }

    [SerializeField]
    private Vector3 position;
    public Vector3 Position
    {
        get => position;
        set
        {
            position = value;
            positionRaw = new float[] { position.x, position.y, position.z };
        }
    }

    [SerializeField]
    private Vector3 rotation;
    public Vector3 Rotation
    {
        get => rotation;
        set
        {
            rotation = value;
            rotationRaw = new float[] { rotation.x, rotation.y, rotation.z };
        }
    }

    [SerializeField]
    private Vector3 scale;
    public Vector3 Scale
    {
        get => scale;
        set
        {
            scale = value;
            scaleRaw = new float[] { scale.x, scale.y, scale.z };
        }
    }
    #endregion

    [OnDeserialized]
    internal void OnDeserializedMethod(StreamingContext context)
    {
        position = new Vector3(positionRaw[0], positionRaw[1], positionRaw[2]);
        rotation = new Vector3(rotationRaw[0], rotationRaw[1], rotationRaw[2]);
        scale = new Vector3(scaleRaw[0], scaleRaw[1], scaleRaw[2]);

        if(materialData == null)
            materialData = new MaterialData();        
    }
}
