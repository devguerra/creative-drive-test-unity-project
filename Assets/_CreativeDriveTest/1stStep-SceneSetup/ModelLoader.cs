﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ModelLoader : MonoBehaviour
{
    private const string WAIT_MESSAGE = "Initializing Interactable Models";

    [SerializeField]
    private SceneSetupLoader sceneSetupLoader;
    [SerializeField]
    private InteractableModelFactory interactableModelFactory;
    [SerializeField]
    private Transform interactableModelRoot;

    public event EventHandler<ModelLoaderEventArgs> OnModelsLoaded;

    private void Start()
    {
        sceneSetupLoader.OnSetupLoaded += OnSetupLoaded;
    }

    public GameObject Duplicate(InteractableModel model)
    {
        var duplicate = GameObject.Instantiate(model.gameObject);

        duplicate.name = duplicate.name.Replace("(Clone)", "");

        duplicate.transform.SetParent(interactableModelRoot, false);

        duplicate.transform.position = Vector3.zero;

        var interactableModel = duplicate.GetComponent<InteractableModel>();

        OnModelsLoaded?.Invoke(
            this,
            new ModelLoaderEventArgs(new InteractableModel[] { interactableModel })
        );

        return duplicate;
    }

    private void OnSetupLoaded(SetupData setupData)
    {
        StartCoroutine(LoadModelsCoroutine(setupData));
    }

    private IEnumerator LoadModelsCoroutine(SetupData setupData)
    {
        WaitScreen.Instance.Show(WAIT_MESSAGE);

        List<InteractableModel> eventList = new List<InteractableModel>();

        foreach (var modelSetup in setupData.Models)
        {
            yield return LoadModelFromResourcesCoroutine(modelSetup, eventList);
        }

        OnModelsLoaded?.Invoke(this, new ModelLoaderEventArgs(eventList.ToArray()));

        WaitScreen.Instance.Hide();
    }

    private IEnumerator LoadModelFromResourcesCoroutine(ModelSetup modelSetup, List<InteractableModel> eventList)
    {
        var resourceRequest = Resources.LoadAsync<GameObject>($"{modelSetup.Name}/{modelSetup.Name}");

        yield return resourceRequest;

        GameObject instance = GameObject.Instantiate(resourceRequest.asset as GameObject);

        var model = interactableModelFactory.Create(instance, modelSetup);

        eventList.Add(model);
    }

    public class ModelLoaderEventArgs : EventArgs
    {
        public readonly InteractableModel[] LoadedModelsArray;

        public ModelLoaderEventArgs(InteractableModel[] loadedModelsArray)
        {
            LoadedModelsArray = loadedModelsArray;
        }
    }
}
