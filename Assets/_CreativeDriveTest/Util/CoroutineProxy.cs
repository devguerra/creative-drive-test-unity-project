﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutineProxy : Singleton<CoroutineProxy>
{
    // object stays active through the whole execution
    // only to allow unactive objects to perform coroutines
}
