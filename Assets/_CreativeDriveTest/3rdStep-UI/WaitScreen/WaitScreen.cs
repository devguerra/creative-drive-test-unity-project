﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WaitScreen : Singleton<WaitScreen>
{
    [SerializeField]
    private TextMeshProUGUI messageText;

    public void Show(string message)
    {
        messageText.text = message;

        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);

        messageText.text = string.Empty;
    }
}
