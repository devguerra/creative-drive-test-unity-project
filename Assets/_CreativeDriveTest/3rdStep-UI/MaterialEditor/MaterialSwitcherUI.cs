﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MaterialSwitcherUI : MonoBehaviour
{
    private const string TEXTURE_TOGGLE_NAME = "TextureButton";
    private const string COLOR_TOGGLE_NAME = "ColorButton";

    [SerializeField]
    private Image thumbnailImage;
    [SerializeField]
    private TextMeshProUGUI nameText;
    [SerializeField]
    CanvasGroup canvasGroup;

    [SerializeField]
    private ToggleGroup textureToggleGroup;
    [SerializeField]
    private ToggleGroup colorToggleGroup;

    private MaterialSwitcher materialSwitcher;
    public MaterialSwitcher MaterialSwitcher
    {
        get => materialSwitcher;
    }

    public void Initialize(MaterialSwitcher materialSwitcher, Action OnComplete)
    {
        this.materialSwitcher = materialSwitcher;

        canvasGroup.alpha = 0;

        InitializeTogglesAccordingToMaterialData(materialSwitcher);

        materialSwitcher.GetThumbnail(
            (thumbnail) =>
            {
                SetThumbnailAndName(thumbnail, materialSwitcher.InteractableModel.ModelSetup.Name);

                canvasGroup.alpha = 1;

                OnComplete();
            }
        );
    }

    private void InitializeTogglesAccordingToMaterialData(MaterialSwitcher materialSwitcher)
    {
        transform.Find($"{TEXTURE_TOGGLE_NAME}{materialSwitcher.InteractableModel.ModelSetup.MaterialData.TextureIndex}").GetComponent<Toggle>().isOn = true;
        transform.Find($"{COLOR_TOGGLE_NAME}{materialSwitcher.InteractableModel.ModelSetup.MaterialData.ColorIndex}").GetComponent<Toggle>().isOn = true;
    }

    private void SetThumbnailAndName(Sprite thumbnail, string name)
    {
        thumbnailImage.sprite = thumbnail;
        nameText.text = name;
    }

    public void SelectTexture(int index)
    {
        // do not select when no toggle is selected
        if (!textureToggleGroup.AnyTogglesOn())
            return;

        // check is necessary to know which toggle was set on
        if (textureToggleGroup.ActiveToggles().ToArray()[0].name.Contains(index.ToString()))
        {
            materialSwitcher.SetTexture(index);
        }
    }

    public void SelectColor(int index)
    {
        // do not select when no toggle is selected
        if (!colorToggleGroup.AnyTogglesOn())
            return;

        // check is necessary to know which toggle was set on
        if (colorToggleGroup.ActiveToggles().ToArray()[0].name.Contains(index.ToString()))
        {
            materialSwitcher.SetColor(index);
        }
    }
}
