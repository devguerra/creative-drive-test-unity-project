﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialSwitcherUIFactory : MonoBehaviour
{
    [SerializeField]
    private GameObject materialSwitcherUIPrefab;
    [SerializeField]
    private Transform verticalLayoutTransform;

    public void Create(MaterialSwitcher materialSwitcher, Action<MaterialSwitcherUI> OnCreate)
    {
        var instance = Instantiate(materialSwitcherUIPrefab, verticalLayoutTransform, false);

        var materialSwitcherUI = instance.GetComponent<MaterialSwitcherUI>();

        materialSwitcherUI.Initialize(materialSwitcher, () =>
        {
            OnCreate(materialSwitcherUI);
        });        
    }
}
