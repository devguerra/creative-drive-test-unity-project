﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MaterialEditor : MonoBehaviour
{
    private const string WAIT_MESSAGE = "Initializing Material Editor";

    [SerializeField]
    private MaterialSwitcherUIFactory materialSwitcherUIFactory;

    [SerializeField]
    private ModelLoader modelLoader;

    private List<MaterialSwitcherUI> materialSwitcherUIList;    

    public void Toggle(bool active)
    {
        gameObject.SetActive(active);
    }

    private void UpdateMaterialSwitcherUIs(InteractableModel[] loadedModelArray)
    {
        CoroutineProxy.Instance.StartCoroutine(UpdateMaterialSwitcherUIsCoroutine(loadedModelArray));
    }

    private IEnumerator UpdateMaterialSwitcherUIsCoroutine(InteractableModel[] loadedModelArray)
    {
        var materialSwitcherArray = Array.ConvertAll<InteractableModel, MaterialSwitcher>(
            loadedModelArray,
            (model) =>{
                return model.GetComponent<MaterialSwitcher>();
            }
        );

        foreach (var materialSwitcher in materialSwitcherArray)
        {
            if (!materialSwitcherUIList.Exists(item => item.MaterialSwitcher == materialSwitcher))
            {
                WaitScreen.Instance.Show(WAIT_MESSAGE);

                MaterialSwitcherUI materialSwitcherUI = null;

                materialSwitcherUIFactory.Create(
                    materialSwitcher,
                    (result) =>
                    {
                        materialSwitcherUI = result;
                    }
                );

                do
                {
                    yield return null;
                } while (materialSwitcherUI == null);

                materialSwitcherUIList.Add(materialSwitcherUI);
            }
        }

        WaitScreen.Instance.Hide();
    }

    private void OnModelsLoaded(object sender, ModelLoader.ModelLoaderEventArgs args)
    {
        UpdateMaterialSwitcherUIs(args.LoadedModelsArray);
    }

    private void Start()
    {
        materialSwitcherUIList = new List<MaterialSwitcherUI>();

        modelLoader.OnModelsLoaded += OnModelsLoaded;

        Toggle(false);
    }
}
