﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;

public class ModelViewerUI : MonoBehaviour
{
    [SerializeField]
    private InteractionController interactionController;

    [Header("UI")]
    [SerializeField]
    private GameObject waitScreen;
    [SerializeField]
    private TextMeshProUGUI currentModeText;
    [SerializeField]
    private TextMeshProUGUI modeInstructionsText;

    private Dictionary<InteractionController.InteractionMode, string> modeInstructionsDictionary = new Dictionary<InteractionController.InteractionMode, string>()
    {
        { InteractionController.InteractionMode.Move, "Arraste para mover em X/Z - Ctrl + Arraste para mover em X/Y" },
        { InteractionController.InteractionMode.Rotate, "Arraste para rotacionar em Y/X - Ctrl + Arraste para rotacionar em Y/Z" },
        { InteractionController.InteractionMode.Scale, "Arraste para escalar igualmente em todos os eixos" }
    };

    private Dictionary<InteractionController.InteractionMode, string> modeNamesDictionary = new Dictionary<InteractionController.InteractionMode, string>()
    {
        { InteractionController.InteractionMode.Move, "Translação" },
        { InteractionController.InteractionMode.Rotate, "Rotação" },
        { InteractionController.InteractionMode.Scale, "Escala" }
    };

    void Start()
    {
        waitScreen.SetActive(true);

        OnModeChange(this, new InteractionController.ModeChangeEventArgs(interactionController.CurrentInteractionMode));

        interactionController.OnModeChange += OnModeChange;
    }

    private void OnModeChange(object sender, InteractionController.ModeChangeEventArgs e)
    {
        currentModeText.text = $"Modo Atual: {modeNamesDictionary[e.CurrentMode]}";

        modeInstructionsText.text = modeInstructionsDictionary[e.CurrentMode];
    }
}
