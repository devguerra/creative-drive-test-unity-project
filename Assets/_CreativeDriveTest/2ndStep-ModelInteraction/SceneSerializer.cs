﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSerializer : MonoBehaviour
{
    private const string DEBUG_HEADER = "<b>SceneSerializer:</b>";

    [SerializeField]
    private SceneSetupLoader sceneSetupLoader;
    [SerializeField]
    private Transform interactableObjectRoot;

    SetupData setupData;

    private void Start()
    {
        sceneSetupLoader.OnSetupLoaded += OnSetupLoaded;
    }

    private void OnSetupLoaded(SetupData setupData)
    {
        this.setupData = setupData;
    }

    public void SerializeScene()
    {
        List<ModelSetup> modelSetupList = new List<ModelSetup>();

        foreach (Transform interactable in interactableObjectRoot)
        {
            var interactableModel = interactable.GetComponent<InteractableModel>();

            modelSetupList.Add(interactableModel.GetUpdatedModelSetup());
        }

        setupData.UpdateData(modelSetupList.ToArray());

        CreateSetupFile();
    }

    public void ResetScene()
    {
        try
        {
            File.Delete(GetSceneConfigurationFilePath());
        }
        catch(Exception)
        {

        }

        SceneManager.LoadScene(0);
    }

    private void CreateSetupFile()
    {
        string json = JsonConvert.SerializeObject(setupData, Formatting.Indented);

        string path = GetSceneConfigurationFilePath();

        using (StreamWriter streamWriter = File.CreateText(path))
        {
            streamWriter.Write(json);
        }

        Debug.Log($"{DEBUG_HEADER} saved scene data at {path}");
    }

    public static string GetSceneConfigurationFilePath()
    {
        return $"{Application.persistentDataPath}/SceneData.json";
    }
}
