﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.EventSystems;

public class InteractableModel : MonoBehaviour
{
    [SerializeField]
    private CapsuleCollider capsuleCollider;

    [SerializeField]
    private ModelSetup modelSetup;
    public ModelSetup ModelSetup
    {
        get => modelSetup;
    }

    [SerializeField]
    private ModelLoader modelLoader;

    public void Initialize(GameObject model, ModelSetup modelSetup, ModelLoader modelLoader)
    {
        this.modelLoader = modelLoader;

        model.transform.SetParent(transform, false);
        model.name = $"{modelSetup.Name}-Model";

        var modelCollider = model.GetComponent<CapsuleCollider>();
        capsuleCollider.direction = modelCollider.direction;
        capsuleCollider.radius = modelCollider.radius;
        capsuleCollider.center = modelCollider.center;
        capsuleCollider.height = modelCollider.height;
        modelCollider.enabled = false;

        gameObject.name = modelSetup.Name;

        transform.position = modelSetup.Position;
        transform.rotation = Quaternion.Euler(modelSetup.Rotation);
        transform.localScale = modelSetup.Scale;

        this.modelSetup = modelSetup;
    }

    public void Move(Vector3 movementDelta)
    {
        transform.position += movementDelta;
    }

    public void Rotate(Vector3 rotationDelta)
    {
        transform.Rotate(rotationDelta, Space.Self);
    }

    public void Scale(Vector3 scaleDelta)
    {
        transform.localScale += scaleDelta;
    }

    public GameObject Duplicate()
    {
        return modelLoader.Duplicate(this);
    }

    public ModelSetup GetUpdatedModelSetup()
    {
        modelSetup.Position = transform.position;
        modelSetup.Rotation = transform.rotation.eulerAngles;
        modelSetup.Scale = transform.localScale;

        return modelSetup;
    }
}
