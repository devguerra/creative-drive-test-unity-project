﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InteractionController : MonoBehaviour
{
    [Header("Runtime")]
    [SerializeField]
    private InteractableModel selectedInteractableModel;
    [SerializeField]
    private InteractionMode currentInteractionMode;
    public InteractionMode CurrentInteractionMode
    {
        get => currentInteractionMode;
    }

    [Header("Configuration")]
    [SerializeField]
    private LayerMask raycastLayerMask;
    [SerializeField]
    private SceneSerializer sceneSerializer;

    [Space(20)]
    [SerializeField]
    private KeyCode modifierKey;
    [SerializeField]
    private KeyCode moveModeKey;
    [SerializeField]
    private KeyCode rotateModeKey;
    [SerializeField]
    private KeyCode scaleModeKey;
    [SerializeField]
    private KeyCode duplicateKey;
    [SerializeField]
    private KeyCode saveKey;

    [Space(20)]
    [SerializeField]
    private float movementSensitivity;
    [SerializeField]
    private float rotationSensitivity;
    [SerializeField]
    private float scaleSensitivity;
    [SerializeField]
    private float interactionDeadZone;

    public event EventHandler<SelectionEventArgs> OnSelection;
    public event EventHandler<ModeChangeEventArgs> OnModeChange;

    private Vector3 previousMousePosition = Vector3.zero;

    private bool duplicated = false;
    private bool dragging = false;
    private bool saved = false;

    private void Select(GameObject selectedObject)
    {
        if(selectedObject == null)
        {
            selectedInteractableModel = null;
        }
        else
        {
            selectedInteractableModel = selectedObject.GetComponent<InteractableModel>();
        }

        OnSelection?.Invoke(
            this,
            new SelectionEventArgs(selectedInteractableModel)
        );
    }

    private void HandleMouseInput()
    {
        var currentMousePosition = Input.mousePosition;

        var mouseDelta = currentMousePosition - previousMousePosition;

        // on click release
        if (Input.GetMouseButtonUp(0))
        {
            // on click release with no drag
            if (!dragging)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition, Camera.MonoOrStereoscopicEye.Mono);
                RaycastHit hitInfo;

                Physics.Raycast(ray, out hitInfo, float.MaxValue, raycastLayerMask);

                Select(hitInfo.collider != null ? hitInfo.collider.gameObject : null);
            }

            dragging = false;
        }
        // on click down or hold
        else
        {
            if(Input.GetMouseButton(0))
            {
                if (mouseDelta != Vector3.zero)
                {
                    HandleDrag(mouseDelta);

                    dragging = true;
                }
            }
        }

        previousMousePosition = currentMousePosition;
    }

    private void HandleDrag(Vector3 mouseDelta)
    {
        // if there is nothing selected, ignore drag
        if (selectedInteractableModel == null)
            return;

        mouseDelta = ApplyDeadZone(mouseDelta);

        switch (currentInteractionMode)
        {
            case InteractionMode.Move:
            {
                MoveSelectedInteractable(mouseDelta);

                break;
            }
            case InteractionMode.Rotate:
            {
                RotateSelectedInteractable(mouseDelta);

                break;
            }
            case InteractionMode.Scale:
            {
                ScaleSelectedInteractable(mouseDelta);

                break;
            }
            default:
                break;
        }
    }

    private Vector3 ApplyDeadZone(Vector3 mouseDelta)
    {
        mouseDelta.x = Mathf.Abs(mouseDelta.x) < interactionDeadZone ? 0 : mouseDelta.x;
        mouseDelta.y = Mathf.Abs(mouseDelta.y) < interactionDeadZone ? 0 : mouseDelta.y;

        return mouseDelta;
    }

    private void MoveSelectedInteractable(Vector3 mouseDelta)
    {
        Vector3 movementDelta;

        if (Input.GetKey(modifierKey))
        {
            movementDelta = mouseDelta;
        }
        else
        {
            movementDelta = new Vector3(mouseDelta.x, 0, mouseDelta.y);
        }

        movementDelta *= movementSensitivity;

        selectedInteractableModel.Move(movementDelta);
    }

    private void RotateSelectedInteractable(Vector3 mouseDelta)
    {
        Vector3 rotationDelta;

        if (Input.GetKey(modifierKey))
        {
            rotationDelta = new Vector3(mouseDelta.y, mouseDelta.x, 0);
        }
        else
        {
            rotationDelta = new Vector3(0, mouseDelta.x, mouseDelta.y);
        }

        rotationDelta *= rotationSensitivity;

        selectedInteractableModel.Rotate(rotationDelta);
    }

    private void ScaleSelectedInteractable(Vector3 mouseDelta)
    {
        Vector3 scaleDelta;
        
        scaleDelta = new Vector3(mouseDelta.x, mouseDelta.x, mouseDelta.x);        

        scaleDelta *= scaleSensitivity;

        selectedInteractableModel.Scale(scaleDelta);
    }

    private void HandleKeyboardInput()
    {
        if(Input.GetKey(modifierKey) && Input.GetKey(duplicateKey))
        {
            if(!duplicated && selectedInteractableModel != null)
            {
                var duplicate = selectedInteractableModel.Duplicate();

                Select(duplicate);

                duplicated = true;

                return;
            }
        }
        else
        {
            duplicated = false;
        }

        if (Input.GetKey(modifierKey) && Input.GetKey(saveKey))
        {
            if (!saved)
            {
                sceneSerializer.SerializeScene();

                saved = true;

                return;
            }
        }
        else
        {
            saved = false;
        }

        if (Input.GetKeyUp(moveModeKey))
        {
            SetCurrentInteractionMode(InteractionMode.Move);

            return;
        }

        if (Input.GetKeyUp(rotateModeKey))
        {
            SetCurrentInteractionMode(InteractionMode.Rotate);

            return;
        }

        if (Input.GetKeyUp(scaleModeKey))
        {
            SetCurrentInteractionMode(InteractionMode.Scale);

            return;
        }
    }

    private void SetCurrentInteractionMode(InteractionMode mode)
    {
        currentInteractionMode = mode;

        OnModeChange?.Invoke(this, new ModeChangeEventArgs(mode));
    }

    private void Update()
    {
        HandleMouseInput();

        HandleKeyboardInput();
    }

    private void Start()
    {
        SetCurrentInteractionMode(InteractionMode.Move);
    }

    public class SelectionEventArgs : EventArgs
    {
        public readonly InteractableModel SelectedInteractable;

        public SelectionEventArgs(InteractableModel selectedInteractable)
        {
            this.SelectedInteractable = selectedInteractable;
        }
    }

    public class ModeChangeEventArgs : EventArgs
    {
        public readonly InteractionMode CurrentMode;

        public ModeChangeEventArgs(InteractionMode currentMode)
        {
            this.CurrentMode = currentMode;
        }
    }

    public enum InteractionMode
    {
        Move,
        Rotate,
        Scale
    }
}
