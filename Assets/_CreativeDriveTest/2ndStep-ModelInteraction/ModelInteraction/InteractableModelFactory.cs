﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableModelFactory : MonoBehaviour
{
    [SerializeField]
    private GameObject interactableModelPrefab;
    [SerializeField]
    private Transform interactableModelRoot;
    [SerializeField]
    private ModelLoader modelLoader;

    public InteractableModel Create(GameObject model, ModelSetup modelSetup)
    {
        var gameObject = Instantiate(interactableModelPrefab, interactableModelRoot);

        var interactableModel = gameObject.GetComponent<InteractableModel>();

        interactableModel.Initialize(model, modelSetup, modelLoader);

        return interactableModel;
    }
}
