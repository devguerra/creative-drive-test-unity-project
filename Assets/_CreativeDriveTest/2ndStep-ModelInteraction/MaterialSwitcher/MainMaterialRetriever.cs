﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMaterialRetriever : MonoBehaviour
{
    [SerializeField]
    private MeshRenderer mainMaterialMeshRenderer;
    public Material Material
    {
        get => mainMaterialMeshRenderer.material;
    }
}
