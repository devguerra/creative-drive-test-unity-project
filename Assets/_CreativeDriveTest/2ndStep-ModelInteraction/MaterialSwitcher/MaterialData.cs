﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
[JsonObject(MemberSerialization = MemberSerialization.OptIn)]
public class MaterialData
{
    [SerializeField, JsonProperty]
    public int TextureIndex = 0;

    [SerializeField, JsonProperty]
    public int ColorIndex = 0;
}
