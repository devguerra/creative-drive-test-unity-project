﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialSwitcher : MonoBehaviour
{
    private const string ALBEDO_TEXTURE_NAME = "_MainTex";

    [SerializeField]
    private InteractableModel interactableModel;
    public InteractableModel InteractableModel
    {
        get => interactableModel;
    }

    private MainMaterialRetriever mainMaterialRetriever;

    public void GetThumbnail(Action<Sprite> OnComplete)
    {
        StartCoroutine(GetThumbnailCoroutine(OnComplete));
    }

    public void SetColor(int index)
    {
        WaitScreen.Instance.Show($"Changing Color of {gameObject.name} to option {index + 1}");

        GetColor(index, (material) =>
        {
            mainMaterialRetriever.Material.color = material.color;

            interactableModel.ModelSetup.MaterialData.ColorIndex = index;

            WaitScreen.Instance.Hide();
        });
    }

    public void SetTexture(int index)
    {
        WaitScreen.Instance.Show($"Changing Texture of {gameObject.name} to option {index + 1}");

        GetTexture(index, (texture) =>
        {
            mainMaterialRetriever.Material.SetTexture(ALBEDO_TEXTURE_NAME, texture);

            interactableModel.ModelSetup.MaterialData.TextureIndex = index;

            WaitScreen.Instance.Hide();
        });
    }

    private IEnumerator GetThumbnailCoroutine(Action<Sprite> OnComplete)
    {
        string name = interactableModel.ModelSetup.Name;
        string thumbnailPath = $"{name}/{name}-Thumbnail";

        yield return LoadAssetFromResources(thumbnailPath, OnComplete);
    }

    private void GetTexture(int index, Action<Texture2D> OnComplete)
    {
        StartCoroutine(GetTextureCoroutine(index, OnComplete));
    }

    private IEnumerator GetTextureCoroutine(int index, Action<Texture2D> OnComplete)
    {
        string texturePath = $"{interactableModel.ModelSetup.Name}/Textures/{index}";

        yield return LoadAssetFromResources(texturePath, OnComplete);
    }

    private void GetColor(int index, Action<Material> OnComplete)
    {
        StartCoroutine(GetColorCoroutine(index, OnComplete));
    }

    private IEnumerator GetColorCoroutine(int index, Action<Material> OnComplete)
    {
        string colorPath = $"{interactableModel.ModelSetup.Name}/Colors/{index}";

        yield return LoadAssetFromResources(colorPath, OnComplete);
    }

    private IEnumerator LoadAssetFromResources<T>(string path, Action<T> OnComplete) where T : UnityEngine.Object
    {
        var resourceRequest = Resources.LoadAsync<T>(path);

        yield return resourceRequest;

        T asset = resourceRequest.asset as T;

        OnComplete(asset);
    }

    private void Start()
    {
        mainMaterialRetriever = gameObject.GetComponentInChildren<MainMaterialRetriever>();

        SetColor(interactableModel.ModelSetup.MaterialData.ColorIndex);
        SetTexture(interactableModel.ModelSetup.MaterialData.TextureIndex);
    }
}
