# Teste de Unity - Creative Drive #

Seja bem vindo à documentação do teste de Unity da Creative Drive desenvolvido por Vinícius Guerra Cardoso.

## Conteúdo do Readme ##

* Downloads
* Instruções de uso
* Estrutura do projeto
* Técnicas notáveis
* Considerações

## Downloads ##

* [Versão x64](https://drive.google.com/file/d/1GZoZkvMPyZSurrJvzJ6EIq2qkXSR8rNc/view?usp=sharing)
* [Versão x86](https://drive.google.com/file/d/1v2mQ5jX5WiQz0s72ChxbPR07y-kE84sO/view?usp=sharing)

## Instruções de uso ##

Clique em um dos modelos para selecioná-lo ou clique no grid (fora dos modelos) para deselecionar. Ao clicar para selecionar um modelo, arraste com o mouse para executar o comando selecionado, indicado no painel no canto superior esquerdo da tela. Use as teclas indicadas para mudar a interação com o mouse e emitir comandos. Clique no botão de salvar cena para salvar localmente os dados da cena. Caso a aplicação seja fechada, ela carregará os últimos dados salvos. Clique no botão de resetar para deletar os dados locais e voltar a carregar os dados da API online. Clique no Editor de Materiais para selecionar texturas e cores para os objetos em cena e clique no X no canto superior direito do Editor de Materiais para fechá-lo.

## Estrutura do projeto ##

Dentro da pasta Assets do projeto, o desenvolvimento foi dividido conforme as etapas descritas no teste para facilitar a avaliação.

O diretório ```_CreativeDriveTest/1stStep-SceneSetup``` contém:
* Carregamento remoto do JSON de configuração através do link providenciado
* Carregamento local do JSON depois de serializada a cena
* Carregamento assíncrono dos modelos através de Resources e inicialização do seu Transform conforme dados de configuração

O diretório ```_CreativeDriveTest/2ndStep-ModelInteraction``` contém:
* Inicialização dos modelos carregados para que sejam interativos
* Classe de controle para selecionar, movimentar, rotacionar, escalar e duplicar os modelos interativos
* Classe para customização dos materiais dos modelos interativos, permitindo selecionar 3 versões das cores e texturas dos modelos
* Classe para recuperar dados atualizados da cena e serializá-los em um arquivo em disco conforme estrutura baixada originalmente

O diretório ```_CreativeDriveTest/3rdStep-UI``` contém:
* UI indicando carregamento e qual processo está sendo desenvolvido
* Editor de Material com uma entrada para cada modelo, mostrando seu thumbnail e permitindo mudar textura Albedo e cor Albedo em runtime através de assets carregados assíncronos de Resources

Fora do diretório ```_CreativeDriveTest/```, na raíz de Assets, estão os plugins Newtonsoft.Json, TextMeshPro e os assets baixados da Asset Store, que podem ser encontrados nos links a seguir:
* https://assetstore.unity.com/packages/3d/vehicles/land/arcade-free-racing-car-161085
* https://assetstore.unity.com/packages/3d/vehicles/land/hq-racing-car-model-no-1203-139221
* https://assetstore.unity.com/packages/3d/vehicles/land/shaded-free-retro-car-179873
* https://assetstore.unity.com/packages/3d/vehicles/land/hq-racing-car-model-no-1202-128064
* https://assetstore.unity.com/packages/3d/vehicles/land/hd-low-poly-racing-car-no-1201-118603

Também foi utilizado um shader de blur para melhorar leitura da UI, encontrado em https://stackoverflow.com/questions/29030321/unity3d-blur-the-background-of-a-ui-canvas

## Técnicas Notáveis ##

### Factory ###

O design pattern Factory foi utilizado para facilitar instanciar e inicializar Assets. Os scripts ```InteractableModelFactory``` e ```MaterialSwitcherUIFactory``` centralizam lógica de criação, configuração e injeção de dependência de seus respectivos produtos.

### Singleton ###

O design pattern Singleton foi usado com parcimônia. Um exemplo de sua implementação é na ferramenta ```CoroutineProxy``` que permite execução de código assíncrono em objetos desabilitados. A referência para esta classe não é importante e pode ser feita através do Singleton. É fácil superutilizar este design pattern, levando a código acoplado e difícil de manter, então optou-se por outras técnicas como referências serializadas e injeção de dependência para a maioria do projeto.

### Eventos ###

Para código desacoplado, optou-se por utilizar eventos implementando EventHandler com EventArgs customizados referentes a cada evento. Este recurso permite código de inicialização, por exemplo, ser executado e seguido por lógica que não precisa estar descrita nas classes onde ficam os eventos, facilitando assim melhor 

### Interação com os modelos ###

As modificações no Transform do modelo são executadas pela classe ```InteractableModel```. Esta classe reside em um prefab único, instanciado e configurado pela Factory e utilizado em todos os modelos, permitindo unificar a lógica em uma classe para quaisquer modelos disponibilizados ou a especialização para lidar com casos especiais.
O input e emissão desses comandos de interação são gerenciados pela classe ```InteractionController```. Essa separação permite a configuração de eventos e parâmetros sem influência na interação com os modelos em si.
A classe ```MaterialSwitcher```, por sua vez, centraliza os comandos de customização, permitindo uma interação com sua UI específica sem que haja acesso a partes irrelevantes. Os dados de material também são serializados e adicionados na configuração inicial, contendo um valor default para o carregamento inicial, mas estando presente em salvamentos consecutivos e fazendo uma especialização da estrutura em JSON providenciada originalmente, deixando claro onde ficam os dados de customização de material.
Esta divisão de responsabilidades é importante para desacoplar o código no projeto e foi planejada logo no início. Documentos de requisitos ou planejamento em UML não estão presentes por conta do tempo limitado de desenvolvimento do teste, mas poderiam explicar claramente a arquitetura, que apesar de ter alguns pontos importantes, é simples.

## Considerações ##

As classes ```MaterialSwitcher``` e ```MainMaterialRetriever``` foram o suficiente para permitir a customização dos modelos utilizados, mas pode-se fazer uma lógica mais elaborada, com uma cadeia de herança e polimorfismo para a customização de modelos com diferentes parâmetros conforme dados de configuração salvos em ScriptableObjects.

A seleção dos objetos poderia ser indicada por um shader de glow ou algum outro indicador, mas não foi possível a implementação para este teste. A seleção também falha as vezes caso haja movimento no momento do clique pois a lógica considera como arrastar sem um objeto selecionado. Ao clicar no vazio, ocorre também de deselecionar o objeto selecionado.

O arquivo contendo a configuração local se chama SceneData.json e se encontra em ```C:\Users\{nome-do-usuário}\AppData\LocalLow\Vinícius Guerra Cardoso\Creative Drive Test```. O botão de resetar cena deleta este arquivo e recarrega a cena, voltando ao carregamento dos dados providenciados através da API online.

A unidade de medida em metros é expressa para o usuário pelo grid no chão. [Este modelo](https://assetstore.unity.com/packages/3d/vehicles/land/hq-racing-car-model-no-1202-128064) é similar a um Porsche 911, que conforme dados do Google, tem a largura de 4.5 metros. O modelo ocupa entre 4 e 5 quadrados, expressando a medida aproximada do modelo.

As coordenadas cartesianas com Y para cima podem ser verificadas pelo usuário conforme descrição dos controles de movimentação e rotação, em que o input em Y move e rotaciona os modelos referentes ao eixo Y.
